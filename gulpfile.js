var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    htmlmin = require('gulp-htmlmin'),
    templateCache = require('gulp-angular-templatecache');

    //build all tasks
    gulp.task('build', ['build-dist', 'build-services-min'], function(){});

    gulp.task('build-dist',function(){
        var componentsCss = [
            'lib/simditor/styles/simditor.css',
            'src/templates/simditor-custom.css',
            'lib/intl-tel-input/build/css/intlTelInput.css'
        ];

        gulp.src(componentsCss)
            .pipe(concat('concat.css'))
            .pipe(rename('ngkeps.css'))
            .pipe(gulp.dest('dist'));

        gulp.src(['src/templates/*.html','src/templates/**/*.html'])
            .pipe(htmlmin({collapseWhitespace: true}))
            .pipe(templateCache({module:'ngKeps', root:'../templates/'}))
            .pipe(gulp.dest('src/config'));

        var componentsJs = [
            'src/ngKeps-services.js',
            'src/config/templates.js',
            'src/directives/*.js',
            'src/services/*.js',
            'src/input-services/*.js',
            'src/load-dependencies-async.js'
        ];

        gulp.src(componentsJs)
            .pipe(concat('concat.js'))
            .pipe(rename('ngkeps.js'))
            .pipe(gulp.dest('dist'))
            .pipe(uglify())
            .pipe(rename('ngkeps.min.js'))
            .pipe(gulp.dest('dist'));

        componentsJs = [
            'lib/simple-module/lib/module.js',
            'lib/simple-hotkeys/lib/hotkeys.js',
            'lib/simditor/lib/simditor.js',
            'lib/ng-simditor/dist/js/ng-simditor.js',
            'src/ngKeps.js',
            'src/config/*.js',
            'src/directives/*.js',
            'src/services/*.js',
            'src/input-services/*.js',
            'src/load-dependencies-async.js'
        ];

    	return gulp.src(componentsJs)
    		.pipe(concat('concat.js'))
    		.pipe(rename('ngkeps-simditor.js'))
    		.pipe(gulp.dest('dist'))
            .pipe(uglify())
            .pipe(rename('ngkeps-simditor.min.js'))
            .pipe(gulp.dest('dist'));
    });

    //build just the ngkeps services into dist
    gulp.task("build-dist-services", function(){
        var componentsCss = [
            'lib/simditor/styles/simditor.css',
            'lib/intl-tel-input/build/css/intlTelInput.css'
        ];

        gulp.src(componentsCss)
            .pipe(concat('concat.css'))
            .pipe(rename('ngkeps.css'))
            .pipe(gulp.dest('dist'));

        gulp.src(['src/templates/*.html','src/templates/**/*.html'])
            .pipe(templateCache({module:'ngKeps', root:'../templates/'}))
            .pipe(gulp.dest('src/config'));

        var componentsJs = [
            'src/ngKeps-services.js',
            'src/config/templates.js',
            'src/directives/*.js',
            'src/services/*.js',
            'src/input-services/*.js',
            'src/load-dependencies-async.js'
        ];

        gulp.src(componentsJs)
            .pipe(concat('concat.js'))
            .pipe(rename('ngkeps.js'))
            .pipe(gulp.dest('dist'))
            .pipe(uglify())
            .pipe(rename('ngkeps.min.js'))
            .pipe(gulp.dest('dist'));
    });

    gulp.task("build-services-min", function(){
        gulp.src(['src/ngKeps-services.js','src/services/*.js'])
            .pipe(concat('services.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('dist'));
    });
