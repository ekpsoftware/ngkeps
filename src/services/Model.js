"use strict";

angular.module('ngKeps')

.service('$nkModelService', [

  '$http',
  '$q',

  function($http, $q) {

    var modelsQ = $q.defer();
    var models = {};

    $http.get('/api/models').then(function(response) {
      var models = response.data;
      modelsQ.resolve(models);
    });

    var getModels = function() {
        return modelsQ.promise;
    };

    return {
        getModels:getModels,
    };
}]);