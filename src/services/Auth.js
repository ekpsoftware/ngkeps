(function() {
var logoutHref = null;
var rootScope = {};
var apiPrefix = '/api/v1/';
var placeTokenInQueryString = false;

angular.module('ngKeps').provider('$nkAuthService', [function () {

  this.setApiPrefix = function(value) {
    apiPrefix = value;
  };

  this.setLogoutHref = function(value) {
    logoutHref = value;
  };

  this.placeTokenInQueryString = function() {
    placeTokenInQueryString = true;
  };

  this.$get = [

  '$http',
  '$location',
  '$window',
  '$q',
  '$nkDataService',
  '$nkModalService',
  '$rootScope',
  '$compile',
  '$log',

  function($http, $location, $window, $q, $nkDataService, $nkModalService, $rootScope, $compile, $log) {
    var publicMembers = {};
    var user = false;
    var webalias = $window.location.host;
    var userDeferred = $q.defer();

    publicMembers.showModal = function(cb) {
      if (user) {
        cb(user);
      } else {
        newElement = $compile("<authorizationmodal></authorizationmodal>")($rootScope);
        $nkModalService.show({content:newElement, source:'angular-element', close:function() {
          cb(user);
        }});
      }
    };

    function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    publicMembers.resetModal = function(token) {
      token = token || getParameterByName('token');
      newElement = $compile("<authorizationmodal token=\""+token+"\" type=\"reset\"></authorizationmodal>")($rootScope);
      $nkModalService.show({content:newElement, source:'angular-element', close:function() {
      }});
    };

    publicMembers.start = function(scope){
      rootScope = scope;
      window.$http = $http;
      try{
        if($window.localStorage[webalias+'-user'])
          user = JSON.parse($window.localStorage[webalias+'-user']);
      } catch (e){
        console.error(e);
      }
      if(user){
        $http.get(apiPrefix + 'users/me')
        .then(function(response){
          user = response.data;
          if (user) {
            if (user.token) {
              $window.localStorage.setItem(webalias+'-user', JSON.stringify(user));
            }
            userDeferred.resolve(user);
            scope.user = user;
          } else {
            userDeferred.reject(user);
          }
        }, function(err){
          userDeferred.reject(user);
        });
        if (user.tokenExpires > (new Date()).getTime()) {
          scope.user = user;
          userDeferred.resolve(user);
        }
      } else {
        userDeferred.reject(user);
      }
      var parser = document.createElement('a');
      parser.href = apiPrefix;

      $http.get(parser.origin + '/api/config')
      .then(function(response){
        $rootScope.config = response.data;
      }, function(err){
        $log.log('Could not get server config');
      });

      return userDeferred.promise;
    };

    publicMembers.getUserPromise = function() {
      return userDeferred.promise;
    };

    publicMembers.getUser = function(){
      return user;
    };

    publicMembers.getToken = function(){
      if($window.localStorage[webalias+'-user']){
        var user = JSON.parse($window.localStorage[webalias+'-user']);
        if (user.token) {
          return user.token;
        }
      }
      return null;
    };

    publicMembers.loginWithProvider = function(provider, data) {
      var loginDeferred = $q.defer();

      if (provider === 'local') {
        $http.post(apiPrefix + 'users/signin', data)
        .then(function(data) {
          if(data.data === false){
            loginDeferred.reject();
          }else{
            var usr = data.data;
            usr.expiration = Date.now();
            $window.localStorage.setItem(webalias+'-user', JSON.stringify(usr));
            user = usr;
            rootScope.user = user;
            loginDeferred.resolve(usr);
          }
        }, function(err) {
          loginDeferred.reject(err.data);
        });
      } else {
        location.href = apiPrefix+'oauths/'+provider;
      }

      return loginDeferred.promise;
    };

    publicMembers.signupWithProvider = function(provider, data, customRoute) {
      var loginDeferred = $q.defer();
      var route = apiPrefix + customRoute || 'users/signup';
      if (provider === 'local') {
        $http.post(route, data)
        .then(function(success) {
          var usr = success.data;
          if (usr === false) {
            loginDeferred.reject();
          } else {
            usr.expiration = Date.now();
            $window.localStorage.setItem(webalias+'-user', JSON.stringify(usr));
            user = usr;
            rootScope.user = user;
            loginDeferred.resolve(usr);
            }
          }, function(err) {
            loginDeferred.reject(err);
          });
      } else {
        location.href = '/auth/'+provider;
      }

     return loginDeferred.promise;
    };

    publicMembers.logout =function(){
      delete $window.localStorage[webalias + '-user'];
      user = false;
    };

    publicMembers.switchUser = function(usr) {
      var defer = $q.defer();
      //if (typeof usr === 'object' && usr.token) {
       // $http.get(apiPrefix + 'users/me')
        //.then(function(response){
        setTimeout(function(){
          $window.localStorage.setItem(webalias+'-user', JSON.stringify(usr));
          user = usr;
          rootScope.user = user;
          defer.resolve(user);
        },15);
        //}, function(err){
        //  defer.reject('invalid user token');
       /// });
      //} else {
       // defer.reject('invalid user object');
      //}
      return defer.promise;
    };

    publicMembers.getPaymentToken = function(){
      var token = $q.defer();
      $nkDataService.get("customers/token")
      .then(function(success){
        token.resolve(success.data);
      }, function(err){
        token.reject(err);
      });
      return token.promise;
    };

    publicMembers.checkout = function(){
      var transaction = $q.defer();
      $nkDataService.post("customers/checkoutWithPaymentMethod")
      .then(function(success){
        transaction.resolve(success.data);
      }, function(err){
        transaction.reject(err);
      });
      return transaction.promise;
    };

    publicMembers.checkPaymentMethod = function(){
      var method = $q.defer();
      $nkDataService.get('customers/checkPaymentMethod')
      .then(function(success){
        method.resolve(success.data);
      }, function(err){
        method.reject(err);
      });
      return method.promise;
    };

    publicMembers.firstTimeCheckout = function(nonce){
      var checkout = $q.defer();
      $nkDataService.post('customers/firstTimeCheckout', {'payment_method_nonce':nonce})
      .then(function(success){
        checkout.resolve(success.data);
      }, function(err){
        checkout.rejeoct(err);
      });
      return checkout.promise;
    };

    return publicMembers;
  }];

}])

.config(['$httpProvider', function($httpProvider) {
  $httpProvider.interceptors.push(['$q', '$location', '$window', function ($q, $location, $window) {
    var webalias = $window.location.host;

    return {
       request: function (config) {
          var url = document.createElement('a');
          url.href = config.url;
          var prefixurl = document.createElement('a');
          prefixurl.href = apiPrefix;
          if (url.href.indexOf(prefixurl.href) === 0) {
            config.headers = config.headers || {};
            try{
              if($window.localStorage[webalias+'-user']){
                var user = JSON.parse($window.localStorage[webalias+'-user']);
                if (user.token) {
                  config.headers.Authorization = 'Bearer ' + user.token;
                  if (placeTokenInQueryString) {
                    if(config.url.indexOf('?') > -1){
                      config.url = config.url + '&access_token=' + user.token;
                    }else{
                      config.url = config.url + '?access_token=' + user.token;
                    }
                  }
                }
              }
            } catch (e){
              console.error(e);
            }
            if(config.url.indexOf('?') > -1){
              config.url = config.url + '&cache_bust=' + (new Date()).getTime();
            }else{
              config.url = config.url + '?cache_bust=' + (new Date()).getTime();
            }
          }
          return config;
       },
       response: function(res) {
        // should be urlPrefix
        var url = document.createElement('a');
        url.href = res.config.url;
        var prefixurl = document.createElement('a');
        prefixurl.href = apiPrefix;
        if (url.href.indexOf(prefixurl.href) === 0) {
          console.log(res.config.url);
          if (res.headers('x-user-token-refresh')) {
            try{
                var token = decodeURIComponent(res.headers('x-user-token-refresh'));
                rootScope.user = JSON.parse(token);
                $window.localStorage.setItem(webalias+'-user', token);
            } catch(err){
              alert(err);
            }
          }
        }
        return res;
       },
       responseError: function (response) {
         if (response.status === 401) {
          console.log('401', response.headers('x-user-logout'));
            if (response.headers('x-user-logout')) {
              alert('Your Session has timed out please login again.');
              delete $window.localStorage[webalias + '-user'];
              if (logoutHref) {
                $window.location.href = logoutHref;
              } else {
                $window.location.reload(true);
              }
            }
         }
         return $q.reject(response);
       }
    };
  }]);
}])

.run(['$nkAuthService', '$rootScope', function($nkAuthService, $rootScope) {
  $nkAuthService.start($rootScope);
}]);

})();
