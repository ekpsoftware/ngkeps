/*jshint browser: true */
'use strict';

angular.module('ngKeps')
.service('imageInputService', ['validatorService',
  function(validatorService){
    return function(scope) {
      if(typeof scope.kepsModel === 'object'){
        if(scope.kepsModel !== null) {
          if (scope.kepsModel instanceof File) {
            scope.drawToCanvas(URL.createObjectURL(scope.kepsModel), function() {
              scope.imageStatus = 'Filename: ' + scope.kepsModel.name + ', File size: ' + scope.kepsModel.size;
            });
          } else if (typeof scope.kepsModel.absoluteFilePath !== 'undefined') {
            scope.drawToCanvas(scope.kepsModel.absoluteFilePath, function() {
              scope.imageStatus = 'Filename: ' + scope.kepsModel.fileName + ', File size: ' + scope.kepsModel.fileSize;
            });
          } else if (typeof scope.kepsModel.url !== 'undefined') {
            scope.drawToCanvas(scope.kepsModel.url);
          } else {
            scope.removeImage();
            scope.kepsType.imageUrl = '';
          }
        } else if (!(scope.kepsModel instanceof File)) {
          scope.removeImage();
          scope.kepsType.imageUrl = '';
        }
      }

      scope.kepsType.randomCanvasId = Math.ceil(Math.random() * 9999);

      scope.imageFileChanged = function(evt){
        if(validatorService.fileValidation(scope, evt.target.files[0])){
          scope.kepsType.imageError = false;
          scope.imageStatus = '';
          var filename = evt.target.files[0].name.toLowerCase();

          if (!(filename.includes('.jpg') || filename.includes('.png'))) {
            scope.$apply(function() {
              scope.kepsType.imageError = 'Image filetype not recognized';
            });
            return;
          }

          if (scope.kepsType.size && scope.kepsType.size < evt.target.files[0].size) {
            scope.$apply(function(){
              scope.kepsType.imageError = "File size exceeds maximum allowed: " + scope.kepsType.size +"B";
            });
            return;
          }
            
          //draw image preview
          scope.drawToCanvas(URL.createObjectURL(evt.target.files[0]), function() {
            scope.kepsModel = evt.target.files[0];
            scope.imageStatus = 'Filename: ' + scope.kepsModel.name + ', File size: ' + scope.kepsModel.size;
          });
        }
      };

      scope.previewImageUrl = function(){
        if (scope.kepsType.imageUrl && scope.kepsType.imageUrl.match(/(\S+\.[^/\s]+(\/\S+|\/|))/g)) {
          scope.drawToCanvas(scope.kepsType.imageUrl, function() {
            scope.kepsModel = {
              url: scope.kepsType.imageUrl
            };
          });
        }
      };

      scope.drawToCanvas = function(url, cb){
        var img = new Image();
        img.src = url;
        img.onload = scope.$apply.bind(scope, function() {
          var width = document.createAttribute('width');
          var height = document.createAttribute('height'); 
          var canvas = document.getElementById(scope.kepsType.randomCanvasId);
          var ctx;
          //scale images reasonably
          if(img.width > img.height){ 
            width.value =  (img.width > 300)  ? 300 : img.width;
            height.value = (img.width > 300)  ? img.height/img.width * 300 : img.height;
          }else if(img.height > img.width){
            height.value = (img.height > 300) ? 300 : img.height;
            width.value =  (img.height > 300) ? img.width/img.height * 300 : img.width;
          }
  
          canvas.setAttributeNode(width);
          canvas.setAttributeNode(height);
          ctx = canvas.getContext('2d');
          ctx.drawImage(img,0,0,width.value,height.value);
          ctx.font = "bold 16px serif";
          ctx.fillStyle = "black";
          ctx.strokeStyle = 'white';
          ctx.lineWidth = 2;
          // \u00d7 is multiplication 'x'
          ctx.strokeText(img.height + ' \u00d7 ' + img.width, 10, height.value - 5);
          ctx.fillText(img.height + ' \u00d7 ' + img.width, 10, height.value - 5);
          
          if (typeof cb === 'function') {
            cb()
          }
        });
      };

      scope.removeImage = function(){
        var canvas = document.getElementById(scope.kepsType.randomCanvasId);
        var ctx = canvas.getContext('2d');
        var width = canvas.width;
        var height = canvas.height;

        ctx.clearRect(0, 0, width, height);
        delete scope.kepsModel;
        
        scope.imageStatus = '';
      };
    };
  }]);