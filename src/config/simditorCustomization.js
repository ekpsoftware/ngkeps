angular.module('ngKeps').config([
  '$provide',
  function($provide) {
    // simditor options customize
    $provide.decorator('simditorOptions', [
      '$delegate',
      function(simditorOptions) {
        Simditor.locale = 'en-US';

        var hasProp = {}.hasOwnProperty;
        var extend = function(child, parent) {
          for (var key in parent) {
            if (hasProp.call(parent, key)) child[key] = parent[key];
          }
          function ctor() {
            this.constructor = child;
          }
          ctor.prototype = parent.prototype;
          child.prototype = new ctor();
          child.__super__ = parent.prototype;
          return child;
        };

        (function(superClass) {
          extend(KepsTagButton, superClass);

          function KepsTagButton() {
            return KepsTagButton.__super__.constructor.apply(this, arguments);
          }

          KepsTagButton.prototype.name = 'keps-tag';
          KepsTagButton.prototype.icon = 'mark';
          KepsTagButton.prototype.htmlTag = 'span';
          KepsTagButton.prototype._status = function() {};
          KepsTagButton.prototype._init = function() {
            var self = this;
            this.menu = (this.editor.opts.kepsTags || []).map(function(tag) {
              return {
                text: tag,
                param: tag
              };
            });

            this.editor.on('decorate', this.decorate.bind(this));
            this.editor.on('undecorate', this.undecorate.bind(this));

            return KepsTagButton.__super__._init.call(this);
          };

          KepsTagButton.prototype.decorate = function(e, $el) {
            var self = this;
            var newHtml = $el.html().replace(/{{(.*?)}}/g, function(match, p1) {
              return self.newNode(p1).outerHTML;
            });

            $el.html(newHtml);
          };

          KepsTagButton.prototype.undecorate = function(e, $el) {
            $el.find('span.label[contenteditable="false"]').each(function(i, span) {
              span.outerText = '{{' + span.textContent + '}}';
            });
          };

          KepsTagButton.prototype.newNode = function(text) {
            var newNode = document.createElement('span');
            newNode.className = 'simditor-keps-tag label label-primary';
            newNode.setAttribute('contenteditable', 'false');
            newNode.textContent = text;
            return newNode;
          };

          KepsTagButton.prototype.command = function(param) {
            this.editor.selection.insertNode(this.newNode(param));
            return this.editor.trigger('valuechanged');
          };

          Simditor.i18n['en-US']['keps-tag'] = 'Data Tags';
          Simditor.Toolbar.addButton(KepsTagButton);
        })(Simditor.Button);

        (function(superClass) {
          extend(KepsSingleLine, superClass);

          function KepsSingleLine() {
            return KepsSingleLine.__super__.constructor.apply(this, arguments);
          }

          KepsSingleLine.pluginName = 'keps-single-line';

          KepsSingleLine.prototype.opts = {
            singleLine: false
          };

          KepsSingleLine.prototype._init = function() {
            if (!this.opts.singleLine) {
              return;
            }

            var self = this;
            this.editor = this._module;
            this.editor.formatter._allowedTags = [];
            this.editor.formatter._allowedStyles = {};
            this.editor.formatter._allowedAttributes = {};

            this.editor.on('valuechanged', function(e, src) {
              self.editor.selection.save();
              self.editor.formatter.format(self.editor.body);
              self.editor.selection.restore();
            });
          };

          Simditor.connect(KepsSingleLine);
        })(SimpleModule);

        simditorOptions.toolbar = [
          'code',
          'fontScale',
          'blockquote',
          'bold',
          'italic',
          'underline',
          'strikethrough',
          'alignment',
          'ul',
          'ol',
          'link',
          'image',
          'keps-tag'
        ];

        simditorOptions.toolbarFloat = false;
        return simditorOptions;
      }
    ]);

    // taOptions.toolbar = [
    //   ['keps-pre'], ['keps-fontsize'],['quote','bold', 'italics', 'underline', 'strikeThrough'], ['keps-alignment'],
    //   [ 'ul', 'ol', 'redo', 'undo', 'clear'],['html','keps-youtube','keps-link', 'keps-image']
    // ];
  }
]);
